<?php

namespace App\Http\Controllers;
use App\Http\Resources\puskesCollection;
use App\Services\DinkesApiService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function all(DinkesApiService $dinkesApiService)
    {
        $pskCollection = collect($dinkesApiService->getpuskes());
        $daerahCollection = collect($dinkesApiService->getWilayah());

        $puskesWithWilayah = collect($pskCollection)->map(function ($puskes) use ($daerahCollection) {
            $wilayah = $daerahCollection->firstWhere("id_kecamatan", $puskes['kode_kecamatan']);
            $puskes['provinsi'] = ['id_provinsi' => $wilayah['id_provinsi'], 'nama_provinsi' => $wilayah['nama_provinsi']];
            $puskes['kota'] = ['id_kota' => $wilayah['id_kota'], 'nama_kota' => $wilayah['nama_kota']];
            $puskes['kecamatan'] = ['id_kecamatan' => $wilayah['id_kecamatan'], 'nama_kecamatan' => $wilayah['nama_kecamatan']];
            
            return $puskes;
        });

        return response()->json([
            'status' => 'success',
            'code' => 200, 
            'count' => $puskesWithWilayah->count(),
            'data' => $puskesWithWilayah,
        ]);

    }
}
