<?php

namespace App\Services;

use GuzzleHttp\Client;

class DinkesApiService
{

    public function __construct()
    {
        $this->client = new Client();
    }

    public function getpuskes(): array
    {
        $res = $this->client->request('GET', 'https://dinkes.jakarta.go.id/api_rekrutmen/puskes', [
            'headers' => ['Authorization' => 'rahasia']
        ]);

        $dataResponse = $res->getBody()->getContents();
        return $res->getStatusCode()==200 ? json_decode($dataResponse, true)['data'] : [];
    }

    public function getWilayah(): array
    {
        $res = $this->client->request('GET', 'https://dinkes.jakarta.go.id/api_rekrutmen/kecamatan', [
            'headers' => ['Authorization' => 'rahasia']
        ]);

        $dataResponse = $res->getBody()->getContents();
        return $res->getStatusCode()==200 ? json_decode($dataResponse, true)['data'] : [];
    }
}
